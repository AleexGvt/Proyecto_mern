
import './App.css';
import { BrowserRouter as Router, Route, Redirect} from 'react-router-dom';
import Users from './user/pages/Users';
import Places from './places/pages/NewPlace';
const App = () => {
  return <Router>
    <Route path="/" exact>
      <Users />
    </Route>
    <Route path="/places/new" exact>
      <Places />
    </Route>
    <Redirect to="/" />
  </Router>;
};

export default App;
