import React from 'react';

import UsersList from '../components/UsersList';

const Users = () =>{
 const USERS = [
   {
     id: "u1",
     name: "Juan Perez",
     image:
       "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ffthmb.tqn.com%2FLBNDBPGGdHEIeS64roecZXv_EEc%3D%2F1536x1040%2Ffilters%3Afill%2528auto%2C1%2529%2FEP2-IA-60435_R_8x10-56a83bea3df78cf7729d314a.jpg&f=1&nofb=1",
     places: 3,
   },
 ];


   return <UsersList items={USERS} /> 
};
export default Users;